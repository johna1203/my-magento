<?php

require_once 'abstract.php';

// Usage: php template.php --template 01

class Change_Template_Shell extends Mage_Shell_Abstract
{

    public function run()
    {
        $template = $this->getArg('template');

        $template = "demo{$template}";
        $store = NULL;
        $website = NULL;

        /** @var Smartwave_Porto_Model_Import_Cms $cms */
        $cms = Mage::getSingleton('porto/import_cms');
        // Import Static Blocks
        $cms->importCms('cms/block', 'blocks', 1);
        // Import CMS Pages
        $cms->importCms('cms/page', 'pages', 1);

        /** @var Smartwave_Porto_Model_Import_Demoversion $import */
        $import = Mage::getSingleton('porto/import_demoversion');
        $import->importDemoversion($template, $store, $website);

        /** @var Smartwave_Porto_Model_Cssconfig_Generator $config */
        $config = Mage::getSingleton('porto/cssconfig_generator');
        $config->generateCss('settings', $website, $store);
        $config->generateCss('design', $website, $store);
    }
}

$shell = new Change_Template_Shell();
$shell->run();
