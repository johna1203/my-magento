<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Shell
 * @copyright   Copyright (c) 2009 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

require_once 'abstract.php';

/**
 * Magento Log Shell Script
 *
 * @category    Mage
 * @package     Mage_Shell
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Shell_Create_Admin_User extends Mage_Shell_Abstract
{
    /**
     * Run script
     *
     */
    public function run() {
        echo $this->getArg("username") . "test" . PHP_EOL;
        if ($this->getArg('username')
            && $this->getArg('password')
            && $this->getArg('firstname')
            && $this->getArg('lastname')
            && $this->getArg('email')
        ) {
            $username  = $this->getArg('username');
            $password  = $this->getArg('password');
            $firstname = $this->getArg('firstname');
            $lastname  = $this->getArg('lastname');
            $email     = $this->getArg('email');

            try {
                $user = Mage::getModel('admin/user')
                    ->setData(array(
                        'username'  => $username,
                        'firstname' => $firstname,
                        'lastname'  => $lastname,
                        'email'     => $email,
                        'password'  => $password,
                        'is_active' => 1
                    ))->save();

            } catch (Exception $e) {
                echo $e->getMessage();
                exit;
            }

            //Assign Role Id
            try {
                $user->setRoleIds(array(1))//Administrator role id is 1 ,Here you can assign other roles ids
                ->setRoleUserId($user->getUserId())
                    ->saveRelations();

            } catch (Exception $e) {
                echo $e->getMessage();
                exit;
            }

            echo "create user";
            return 0;

        } else {
            echo $this->usageHelp();
            return 1;
        }
    }

    /**
     * Retrieve Usage Help Message
     *
     */
    public function usageHelp() {
        return <<<USAGE
Usage:  php -f create_admin_user.php -- -username

  username          User name
  password          Password
  firstname         First Name
  lastname          Last Name
  email             Email
  help              This help

USAGE;
    }
}

$shell = new Mage_Shell_Create_Admin_User();
$shell->run();
